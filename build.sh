#!/usr/bin/env sh

## SETTINGS ######################################################################################

# prefs
rootcmd="sudo" # program for root privileges, default is sudo although we all know doas is better
compiler="gcc" # replace with tcc or whatever if you think gcc is too bloated

# folders
installDir="/usr/local/bin/" # folder to install Cantina in, default is '/usr/local/bin/'

##################################################################################################


## MAIN VARS #####################################################################################

errormsg="Operation failed! Guru meditation"

##################################################################################################


helpMe() {
    printf "\nQuick 'n' dirty script used to compile the new C Cantina.
Will soon be replaced by a proper Makefile. (I just gotta learn GNU Make)

Usage:  ./build.sh [options]

Options:
  compile           creates binary form of the program and places it
  in the ./bin dir
  install           installs the program to the chosen directory
  (open up the script and you'll understand what this means)

[Author: mirk0dex <mirkodi.tech>]\n\n"
}

folderCheck() {
    case $PWD in
        *cantina-rewrite*|*Cantina-rewrite*) :;;
        *) printf "\nPlease make sure you're in the
        right folder before running this script. Thanks!\n\n";
           exit 100
    esac
    if [ ! -d "$PWD"/bin ]; then # make sure bin directory exists
        mkdir bin
    fi
}

cantinaCompile() {
    folderCheck
    $compiler -o bin/cantina cantina.c -lncurses
}

desktopCopy() {
    if [ ! -d "$HOME/.local/share/applications" ]; then
        mkdir "$HOME/.local/share/applications"
    fi
    cp "$PWD/Cantina.desktop" "$HOME/.local/share/applications/"
}

iconCopy() {
    if [ ! -d "$HOME/.icons" ]; then
        mkdir "$HOME/.icons"
    fi
    cp "$PWD/ico/cantina-icon-48x48.ico" "$HOME/.icons/"
}

XDGStuffs() {
    desktopCopy
    iconCopy
}

cantinaInstall() {
    if ! cantinaCompile; then printf "\n%s\n\n", "$errormsg"; exit 100; fi
    $rootcmd cp bin/cantina "$installDir"
    XDGStuffs
}


## FLAGS #########################################################################################

case $1 in
    compile) if ! cantinaCompile; then printf "\n%s\n\n", "$errormsg"; exit 100; fi;;
    install) if ! cantinaInstall; then printf "\n%s\n\n", "$errormsg"; exit 100; fi;;
    *)       helpMe
esac

##################################################################################################
