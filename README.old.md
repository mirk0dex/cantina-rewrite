
    Copyright (C) 2021 mirk0dex
    <mirk0dex.gitlab.io>
    
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

# Cantina

**THIS IS PRE-ALPHA, WORK-IN-PROGRESS SOFTWARE, NOT YET IN A WORKING STATE.**

In this folder you should find a COPYING file and the cantina.sh script, other than this README.

This script lets you create your very personal archives where you can put old documents, pictures and stuffs without having to get rid of them.
You can then organize them however you want. The archives won't take up as much space as a regular folder filled with files because they will get unzipped and then zipped every time you add something to them.

The program's intended to never exceed 469 SLOC. This might change though, since it's only in its early stages of development.

Thanks to my friend Lore2117 for kindly *automatically generating the Italian word "cantina" and forcing me to make a piece of software based on it* (it's a long story, don't worry too much about it).

## Features

- Archive your old documents and family pictures (that you don't want to delete) in handy zipped containers;
- open and close these containers whenever (and wherever, if you're using a laptop or any kinda mobile device) you want;
- backup the archives to get just a little bit less paranoid about your precious data;
- that's basically it.

## Installation

Just ``git clone https://gitlab.com/mirk0dex/Cantina`` and ``cd`` into the newly created directory. There you go, have fun!
Please make sure you have chmodded +x the script (making it executable) before running it.
Note: you have to solve the dependencies yourself.

## Dependencies

The script has the following deps:

- git (to clone the repo);
- a UNIX-like system with a home folder and a '.config' directory inside it;
- bash;
- yq & jq (needed for the configuration file);
- zip & unzip;
- a text editor (optional, used to configure Cantina);
- A working computer, lol;
- Some free space in your ~ (home folder).

Commands to install them all on various distros and OSes:

*Ubuntu*/*Debian*/*Elementary OS*/*Linux Mint*/Any Ubuntu or Debian-based distro:
```
su -c "apt-get update -y && apt-get upgrade -y && apt-get install -y git bash yq jq zip unzip && echo 'Error: working computer not found! (jk ;))'"
```

*Arch Linux*/*Manjaro*/*Artix Linux*/*Endeavour OS*/*Parabola GNU/Linux*/Any Arch-based distro:
```
su -c "pacman -Syu --noconfirm && pacman -S --noconfirm git bash yq jq zip unzip && echo 'Shrek is love, Shrek is life'"
```

*Void GNU/Linux*:
```
su -c "xbps-install -Syu && xbps-install git bash yq jq zip unzip && echo amogus"
```

*FreeBSD*:
```
su -c "pkg install git bash yq jq zip unzip && echo 'I see you're a person of culture as well'"
```

*OpenBSD*:
```
doas pkg_add git bash yq jq zip unzip && echo "henlo? r u a OpenBaSeD usr?"
```
### Proprietary systems

While *everyone* should *always* use [free software](https://gnu.org/philosophy/free-sw) and OSes, it is possible to use Cantina on a non-free operating system as well.

*MacOS* with *brew*:
```
brew install git bash yq jq zip unzip && echo "Apple sux, don't get mad, it's just how things are bro"
```

## Usage

See './cantina.sh --help' for info on Cantina.

To get started, make sure you are in the 'Cantina' folder and run './cantina.sh'.

## Contributing

See 'CONTRIBUTING.md'.

## To-Do

- Implement logs for easier debugging
- Make the script automatically list the created archives in ~/.config/cantina/archive-list
- Make the program actually work lmfao
