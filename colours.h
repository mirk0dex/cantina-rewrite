// coloUrs
static const char RED[]       = "\033[0;31m";
static const char ORANGE[]    = "\033[0;33m";
static const char CYAN[]      = "\033[0;36m";
static const char LIGHTBLUE[] = "\033[1;94m";
static const char YELLOW[]    = "\033[1;33m";
static const char GREEN[]     = "\033[1;32m";
static const char PURPLE[]    = "\033[0;35m";
static const char LIGHTRED[]  = "\033[1;31m";
static const char NC[]        = "\033[0m";
