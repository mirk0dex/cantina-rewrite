# Cantina (complete rewrite in progress)

**NOTE: This README is temporary.**

Cantina is a useless program that, when complete, will allow you to create and
manage archives. I started, some time ago, writing it in shell, but, as it was
getting too bloated, I recently decided to "reboot" the development, and why
not, maybe write it in C this time. (See [this blog
post](https://mirkodi.tech/blog/cantina-needs-a-rewrite.html) for more info).

# Building

No Makefile cuz I am stupid: I don't know how to write those, sorry about that.
There's, though, a nice lil script called "build.sh" that you can use to compile
this version of Cantina yourself.

Deps:
