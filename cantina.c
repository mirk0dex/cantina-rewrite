/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *\
 *   Copyright (C) 2021 mirk0dex                                               *
 *   <mirkodi.tech>                                                            *
 *                                                                             *
 *   This program is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by      *
 *   the Free Software Foundation, either version 3 of the License, or         *
 *   (at your option) any later version.                                       *
 *                                                                             *
 *   This program is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *   GNU General Public License for more details.                              *
 *                                                                             *
 *   You should have received a copy of the GNU General Public License         *
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.    *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                             *
 * Cantina: C rewrite of the original Cantina script of mine which lets YOU    *
 * (yes, YOU!) create, manage and organize personal compressed archives to put *
 * your henta- I mean, *very important documents* in.                          *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                             *
 * Building deps:                                                                       *
 * a UNIX-like OS (such as GNU/Linux or *BSD), ncurses ("-dev" or "-devel")    *
 *                                                                             *
\* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */


#include <ncurses.h> // + <stdio.h>, which is already included by <ncurses.h>
#include <stdlib.h>  // 4 shell commands
#include <unistd.h>  // 4 FLAGS (getopt)
#include "colours.h" // coloUrs (I am not even bri'ish lol) 4 for text
#include "config.h"  // 4 user settings


// MAIN VARIABLES ////////////////////////////////////////////////////////////////////////////////

// current version of tha program
static const char version[20] = "Pre-alpha 0.0.1";

// vars needed later on
char answer[60]; // used for dir paths, archive names & stuff
int  ch;         // used for options and receiving input in general

//////////////////////////////////////////////////////////////////////////////////////////////////


int main(int argc, char *argv[] /* stuff for getopt (FLAGS) */) {

  // help screen
  void helpMe() {
    printf("\nCantina - CLI tool to archive stuff [Version %s]\n\n", version);
    printf("Usage:\t./cantina.sh\n");
    printf("\t./cantina.sh [options]\n\n");
    printf("Cantina is a program that allows you to create and manage virtual archives (.zip files) that will get unzipped whenever you need to put stuff in them and, when you're done, zipped once again.\n");
    printf("For some more info (but not a lot, just a bit), read the README.md that came with this program.\n\n");
    printf("Cantina was created by the noob programmer mirk0dex <gitlab.com/mirk0dex>. Please don't bully him because of his crappy software development skills.\n");
    printf("Thanks :D.\n\n");
    printf("Options:\n");
    printf("\t-h\tprints this help screen and exits\n");
    printf("\t-v\tprints program version and exits\n\n");
    printf("[Author: mirk0dex <gitlab.com/mirk0dex>]\n\n");
  }


  // FLAGS /////////////////////////////////////////////////////////////////////////////////////////

  while ((ch = getopt(argc, argv, "hv")) != -1) { // get flag (option) via getopt()
    switch (ch) {
        // -h (help)
        case 'H':
        case 'h':
          helpMe();
          exit(0);

        // -v (version)
        case 'V':
        case 'v':
          printf("Current version is: %s\n", version);
          exit(0);

        // if unknown option; then
        default:
          helpMe();
          exit(0);
    }
  }

  //////////////////////////////////////////////////////////////////////////////////////////////////

  // stuffs 4 ncurses
  char item    [27];
  char list[11][27] = {                                  // menu options:
                        {"Create new archive"},          // archiveCreate
                        {"Open an archive"},             // archiveOpen
                        {"Close an archive"},            // archiveClose
                        {"List all archives"},           // archiveList
                        {"Backup an archive"},           // archiveBackup
                        {"Rename an archive"},           // archiveRename
                        {"Delete an archive"},           // archiveDelete
                        {"Update this program"},         // update
                        {"Close all archives and exit"}, // archiveClose all && exit, thanks
                        {"Exit"},                        // do I need to explain this one?
                        {"Help" } };                     // helpMe (killMe)


  void initColour() {
    if (has_colors() == false) {
      fprintf(stderr, "ColoUrs unavailable: not using them\n");
    }
    else {
      start_color();
    }
  }

  void initNcurses() {
    // prepare ncurses
    initscr();                // initialize curses
    raw();                    // disable line bufferin'
    noecho();                 // no character echoing on screen
    initColour();
    curs_set(0);

  }
  initNcurses();

  // ncurses initial TUI
  void printmenu() {
    // print menu items
    int i = 0;
    for (i = 0; i < 11; i++) {
      if (i == 0) {
        attron(A_STANDOUT); // highlight the first item
      }
      else {
        attroff(A_STANDOUT);
        sprintf (item, "%-11s", list[i]);
        mvprintw(i++, 2, "%s", item);
      }
    }
    refresh(); // update screen

    while ((ch = getch()) != 'q') {     // if user clicks 'q'; program stops
      sprintf (item, "%-11s", list[i]);
      mvprintw(i++, 2, "%s", item);

      switch(ch) {
        case KEY_UP:                    // if user clicks up key; then move cursor up
          i--;
          i = (i < 0) ? 4 : i;
          break;

        case KEY_DOWN:                  // if user clicks down key; then move cursor down
          i++;
          i = (i > 4) ? 0 : i;
      }

      attron(A_STANDOUT); // highlight selected item
      sprintf(item, "%-11s", list[i]);
      mvprintw(i++, 2, "%s", item);
      attroff(A_STANDOUT);
    }
  }

  printmenu();

  endwin();
  return 0;
}

  /*void printmenu() {
    printf("\nChoose what to do:\n\n");
    printf("  h. Help\n");
    printf("  %s1. %sCreate new archive\n",        NC, RED);
    printf("  %s2. %sOpen an archive\n",           NC, ORANGE);
    printf("  %s3. %sClose an archive\n",          NC, CYAN);
    printf("  %s4. %sList all archives\n",         NC, LIGHTBLUE);
    printf("  %s5. %sBackup an archive\n",         NC, YELLOW);
    printf("  %s6. %sRename an archive\n",         NC, PURPLE);
    printf("  %s7. %sDelete an archive\n",         NC, LIGHTRED);
    printf("  %s8. %sUpdate this program\n",       NC, GREEN);
    printf("  %s9. Close all archives and exit\n", NC);
    printf("  q. Exit\n");
  }*/

/*  void archiveCreate() {
    printf("\nEnter %sq%s to go back to the initial menu.\n", YELLOW, NC);
    printf("Enter the filename (you usually find the archive in ~/Cantina/Archives/)\nIt cannot contain spaces\n\n");

    do {
      scanf(" %s", &answer);
      if ( answer == NULL ) {
        fprintf(stderr, "Please specify a new filename.");
      }
    } while ( answer == NULL )

    printf("%c", answer);
  }


  // pickable options
   printf("\n");
  scanf(" %d\n", &ch);
  switch (ch) {
     // help
     case 'H':
     case 'h':
       helpMe();
       break;

     case '1':
       archiveCreate();
       break;

     case 2:
       archiveOpen();
       break;

     case 3:
       archiveClose();
       break;

     case 4:
       archiveList();
       break;

     case 5:
       archiveBackup();
       break;

     case 6:
       archiveRename();
       break;

     case 7:
       archiveDelete();
       break;

     // update this program
     case 8:
       update();
       printmenu();
       break;
  } */
